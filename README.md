EtherPixels
===========

EtherPixels is a smart contract that allows users to buy and sell pixels in tiles of 16x16 pixel tiles. Users can modify these tiles as often as they like, and they can also leave a URL and a small text to go along with the tile.

Why not get a tile and link to your home page? The space will be yours forever!

Buying and selling
------------------

Users can either buy a brand new, unused tile, or buy a tile from another user.

Buying new tiles cost a fixed default price. Users selling their tiles can choose whatever price they like. Tiles on page 1 might become lucrative, so users could potentially sell these at a profit down the line.

Tile organisation and pages
---------------------------

Tile are arranged on a fixed, 32 by 32 grid. This is called a page. New pages may be created at any time by the users.

Tile ID
-------

TileID is constructed as follows:

```
0xPPYYXX
```

Where:
 * PP = Page number (1 to infinite, expand as needed)
 * YY = Y coordinate in tile space (0 to PAGE_SIZE)
 * XX = X coordinate in tile space (0 to PAGE_SIZE)

The entire page 0 is reserved and cannot be used, so the first usable page is page 1.

Tile fields
-----------

Each tile has 9 fields:

| Field name      | Description                                                                                                                        |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------|
| pixels          | The raw array of pixels making up this tile.                                                                                       |
| url             | URL the tile is pointing to                                                                                                        |
| title           | Tile text to be displayed when hovering                                                                                            |
| owner           | Address of the owner                                                                                                               |
| approvedAddress | Address that is allowed to make 1 change to the tile (ERC721)                                                                      |
| price           | If this field is not 0, then this tile can be purchased by anyone for the given price. The current owner receives the payment.     |
| next            | ID of the next tile in the owner of this tile's linked list of tiles.                                                              |
| prev            | ID of the previous tile in the owner of this tile's linked list of tiles.                                                          |
| group           | ID of another tile to fetch the url and title from. This allowed us to save gas by not inserting the same text for multiple tiles. |

Notice the `next` and `prev` fields. All tiles are part of a linked list that is associated with the same owner. This makes it possible to easily fetch all tiles owned by the same owner, simply by following the linked list.

Author
======

John Short <john@short.digital>
Copyright © 2019

License
=======

GPLv3
