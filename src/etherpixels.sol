//------------------------------------------------------------------------------
//
// EtherPixels - Buy and trade pixels
//
// Copyright (C) 2019 John Short
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//------------------------------------------------------------------------------

pragma solidity ^0.5.9;

//------------------------------------------------------------------------------
//
// ERC-165 Interfaces
//
//------------------------------------------------------------------------------

interface ERC165
{
	/// @notice Query if a contract implements an interface
	/// @param interfaceID The interface identifier, as specified in ERC-165
	/// @dev Interface identification is specified in ERC-165. This function
	///  uses less than 30,000 gas.
	/// @return `true` if the contract implements `interfaceID` and
	///  `interfaceID` is not 0xffffffff, `false` otherwise
	function supportsInterface(bytes4 interfaceID) external view returns (bool);
}

//------------------------------------------------------------------------------
//
// ERC-721 Interfaces
//
//------------------------------------------------------------------------------
/// @title ERC-721 Non-Fungible Token Standard
/// @dev See https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md
///  Note: the ERC-165 identifier for this interface is 0x80ac58cd
interface ERC721 /* is ERC165 */
{
	/// @dev This emits when ownership of any NFT changes by any mechanism.
	///  This event emits when NFTs are created (`from` == 0) and destroyed
	///  (`to` == 0). Exception: during contract creation, any number of NFTs
	///  may be created and assigned without emitting Transfer. At the time of
	///  any transfer, the approved address for that NFT (if any) is reset to
	///  none.
	event Transfer(
		address indexed _from,
		address indexed _to,
		uint256 indexed _tokenId
	);

	/// @dev This emits when the approved address for an NFT is changed or
	///  reaffirmed. The zero address indicates there is no approved address.
	///  When a Transfer event emits, this also indicates that the approved
	///  address for that NFT (if any) is reset to none.
	event Approval(
		address indexed _owner,
		address indexed _approved,
		uint256 indexed _tokenId
	);

	/// @dev This emits when an operator is enabled or disabled for an owner.
	///  The operator can manage all NFTs of the owner.
	event ApprovalForAll(
		address indexed _owner,
		address indexed _operator,
		bool _approved
	);

	/// @notice Count all NFTs assigned to an owner
	/// @dev NFTs assigned to the zero address are considered invalid, and this
	///  function throws for queries about the zero address.
	/// @param _owner An address for whom to query the balance
	/// @return The number of NFTs owned by `_owner`, possibly zero
	function balanceOf(address _owner) external view returns (uint256);

	/// @notice Find the owner of an NFT
	/// @dev NFTs assigned to zero address are considered invalid, and queries
	///  about them do throw.
	/// @param _tokenId The identifier for an NFT
	/// @return The address of the owner of the NFT
	function ownerOf(uint256 _tokenId) external view returns (address);

	/// @notice Transfers the ownership of an NFT from one address to another
	///  address
	/// @dev Throws unless `msg.sender` is the current owner, an authorized
	///  operator, or the approved address for this NFT. Throws if `_from` is
	///  not the current owner. Throws if `_to` is the zero address. Throws if
	///  `_tokenId` is not a valid NFT. When transfer is complete, this function
	///  checks if `_to` is a smart contract (code size > 0). If so, it calls
	///  `onERC721Received` on `_to` and throws if the return value is not
	///  `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`.
	/// @param _from The current owner of the NFT
	/// @param _to The new owner
	/// @param _tokenId The NFT to transfer
	/// @param data Additional data with no specified format, sent in call to
	///  `_to`
	function safeTransferFrom(
		address _from,
		address _to,
		uint256 _tokenId,
		bytes calldata data
	) external payable;

	/// @notice Transfers the ownership of an NFT from one address to another
	///  address
	/// @dev This works identically to the other function with an extra data
	///  parameter, except this function just sets data to ""
	/// @param _from The current owner of the NFT
	/// @param _to The new owner
	/// @param _tokenId The NFT to transfer
	function safeTransferFrom(
		address _from,
		address _to,
		uint256 _tokenId
	) external payable;

	/// @notice Transfer ownership of an NFT -- THE CALLER IS RESPONSIBLE
	///  TO CONFIRM THAT `_to` IS CAPABLE OF RECEIVING NFTS OR ELSE
	///  THEY MAY BE PERMANENTLY LOST
	/// @dev Throws unless `msg.sender` is the current owner, an authorized
	///  operator, or the approved address for this NFT. Throws if `_from` is
	///  not the current owner. Throws if `_to` is the zero address. Throws if
	///  `_tokenId` is not a valid NFT.
	/// @param _from The current owner of the NFT
	/// @param _to The new owner
	/// @param _tokenId The NFT to transfer
	function transferFrom(
		address _from,
		address _to,
		uint256 _tokenId
	) external payable;

	/// @notice Set or reaffirm the approved address for an NFT
	/// @dev The zero address indicates there is no approved address.
	/// @dev Throws unless `msg.sender` is the current NFT owner, or an
	///  authorized operator of the current owner.
	/// @param _approved The new approved NFT controller
	/// @param _tokenId The NFT to approve
	function approve(address _approved, uint256 _tokenId) external payable;

	/// @notice Enable or disable approval for a third party ("operator") to
	///  manage all of `msg.sender`'s assets.
	/// @dev Emits the ApprovalForAll event. The contract MUST allow
	///  multiple operators per owner.
	/// @param _operator Address to add to the set of authorized operators.
	/// @param _approved True if the operator is approved, false to revoke
	///  approval
	function setApprovalForAll(address _operator, bool _approved) external;

	/// @notice Get the approved address for a single NFT
	/// @dev Throws if `_tokenId` is not a valid NFT
	/// @param _tokenId The NFT to find the approved address for
	/// @return The approved address for this NFT, or the zero address if
	///  there is none
	function getApproved(uint256 _tokenId) external view returns (address);

	/// @notice Query if an address is an authorized operator for another
	///  address
	/// @param _owner The address that owns the NFTs
	/// @param _operator The address that acts on behalf of the owner
	/// @return True if `_operator` is an approved operator for `_owner`,
	///  false otherwise
	function isApprovedForAll(
		address _owner,
		address _operator
	) external view returns (bool);
}

interface ERC721TokenReceiver
{
	/// @notice Handle the receipt of an NFT
	/// @dev The ERC721 smart contract calls this function on the
	///  recipient after a `transfer`. This function MAY throw to revert and
	///  reject the transfer. Return of other than the magic value MUST result
	///  in the transaction being reverted.
	/// @notice The contract address is always the message sender.
	/// @param _operator The address which called `safeTransferFrom` function
	/// @param _from The address which previously owned the token
	/// @param _tokenId The NFT identifier which is being transferred
	/// @param _data Additional data with no specified format
	/// @return
	///  `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`
	///  unless throwing
	function onERC721Received(
		address _operator,
		address _from,
		uint256 _tokenId,
		bytes calldata _data
	) external returns(bytes4);
}

/// @title ERC-721 Non-Fungible Token Standard, optional metadata extension
/// @dev See https://eips.ethereum.org/EIPS/eip-721
///  Note: the ERC-165 identifier for this interface is 0x5b5e139f.
interface ERC721Metadata /* is ERC721 */
{
	/// @notice A descriptive name for a collection of NFTs in this contract
	function name() external view returns (string memory _name);

	/// @notice An abbreviated name for NFTs in this contract
	function symbol() external view returns (string memory _symbol);

	/// @notice A distinct Uniform Resource Identifier (URI) for a given asset.
	/// @dev Throws if `_tokenId` is not a valid NFT. URIs are defined in RFC
	///  3986. The URI may point to a JSON file that conforms to the "ERC721
	///  Metadata JSON Schema".
	function tokenURI(uint256 _tokenId) external view returns (string memory);
}

//------------------------------------------------------------------------------
//
// EtherPixels NFT Contract
//
//------------------------------------------------------------------------------
/// @title EtherPixels
contract EtherPixels is ERC165, ERC721, ERC721Metadata
{
	//--------------------------------------
	//
	// Constants
	//
	//--------------------------------------

	// Tile grid
	// Pixel size in bytes
	uint256 private constant PIXEL_SIZE        = 3;

	// Tile size in pixels (w = TILE_SIZE, h = TILE_SIZE)
	uint256 private constant TILE_SIZE         = 16;
	
	// TILE_SIZE * TILE_SIZE * PIXEL_SIZE
	uint256 private constant TILE_PIXEL_SIZE   = 768;
	
	// Math.ceil(TILE_PIXEL_SIZE / 32)
	uint256 private constant TILE_STORAGE_SIZE = 24;
	
	// Page size in tiles (w = PAGE_SIZE, h = PAGE_SIZE)
	uint256 private constant PAGE_SIZE         = 32;

	// Interface constants
	bytes4 private constant InterfaceID_ERC165         = 0x01ffc9a7;
	bytes4 private constant InterfaceID_ERC721         = 0x80ac58cd;
	bytes4 private constant InterfaceID_ERC721Metadata = 0x5b5e139f;
	bytes4 private constant ERC721_RECEIVED            = 0x150b7a02;

	//--------------------------------------
	//
	// Contract variables
	//
	//--------------------------------------

	// A note on tile IDs:
	//
	// TileID is constructed as follows:
	//     0xPPYYXX
	//
	// Where:
	//     PP = Page number (1 to infinite, expand as needed)
	//     YY = Y coordinate in tile space (0 to PAGE_SIZE)
	//     XX = X coordinate in tile space (0 to PAGE_SIZE)
	//
	// Page number 0 is not allowed.
	//
	// A note on tile's linked list:
	// Each tile is a part of a linked list, to allow us to easily fetch all
	// tiles belonging to a user

	// Tile storage
	mapping(uint256 => mapping(uint256 => bytes32)) private tilePixels;
	mapping(uint256 => string)  private tileUrl;
	mapping(uint256 => string)  private tileTitle;
	mapping(uint256 => address) private tileOwner;
	mapping(uint256 => address) private tileApproval;
	mapping(uint256 => uint256) private tilePrice;
	mapping(uint256 => uint256) private tileNext; // Next tile in linked list
	mapping(uint256 => uint256) private tilePrev;
	mapping(uint256 => uint256) private tileGroup; // Tile ID who's text to use

	// Owner storage
	// Number of tiles owned
	mapping(address => uint256) private ownerTileCount;
	// Reference to first tile in their chain
	mapping(address => uint256) private ownerFirstTile;
	// ERC721 approved address
	mapping(address => mapping(address => bool)) private operatorApprovals;

	// Number of pages
	uint256 private _pageCount;

	// EtherPixel operator
	address private contractOperator;

	// Default price per tile
	uint256 private _price;

	// Fee for on-contract peer-to-peer sale of tiles
	// Fee rate is calcualted as follows: 100 / percent
	// Final fee is calcualted like this: price / (100 / percent) = fee
	uint256 private saleFeeRate;

	// ERC721 metadata base URL
	string private ERC721MetadataName;
	string private ERC721MetadataSymbol;
	string private ERC721MetadataUrl;

	//--------------------------------------
	//
	// Events
	//
	//--------------------------------------

	// When tile content is modified
	event Modified(
		address indexed by,
		uint256 indexed tileId,
		bytes32 indexed action
	);

	//--------------------------------------
	//
	// Constructor
	//
	//--------------------------------------

	/// @notice Constructor
	constructor() public
	{
		ERC721MetadataName = "EtherPixels";
		ERC721MetadataSymbol = "PIXEL";
		ERC721MetadataUrl = "https://etherpixels.page/meta/";
		_price = 1 ether;
		saleFeeRate = 10; // 10% default fee
		contractOperator = msg.sender;
	}

	//--------------------------------------
	//
	// Fallback function
	//
	//--------------------------------------

	/// @notice Fallback function - Does nothing
	function() external
	{
		// There is nothing to do
	}

	//--------------------------------------
	//
	// EtherPixel static functions
	//
	//--------------------------------------

	/// @notice Get the default price for an unclaimed tile
	/// @return price default price for an unclaimed tile
	function getPrice() external view returns (uint256 price)
	{
		return _price;
	}

	/// @notice Get on-contract peer-to-peer sales fee rate
	/// @return Rate
	function getFeeRate() external view returns (uint256 feeRate)
	{
		return saleFeeRate;
	}

	/// @notice Calculate on-contract peer-to-peer sales fee (in wei)
	/// @return Fee
	function calculateFee(uint256 price) external view returns (uint256 fee)
	{
		return price / saleFeeRate;
	}

	/// @notice Get the number of pages in use
	/// @return pageCount number of pages
	function getPageCount() external view returns (uint256 pageCount)
	{
		return _pageCount;
	}

	/// @notice Get the size of a tile bitmap (in bytes)
	/// @return pixelSize size of tile in bytes
	function getTilePixelSize() external pure returns (uint256 pixelSize)
	{
		return TILE_PIXEL_SIZE;
	}

	/// @notice Get the size of a tile (in pixels)
	/// @return tileSize size of tile in pixels
	function getTileSize() external pure returns (uint256 tileSize)
	{
		return TILE_SIZE;
	}

	/// @notice Get the size of a page (in tiles)
	/// @return pageSize size of page in tiles
	function getPageSize() external pure returns (uint256 pageSize)
	{
		return PAGE_SIZE;
	}

	/// @notice Get a tile by location
	/// @param page page
	/// @param x x coordinate of the tile (in tile space)
	/// @param y y coordinate of the tile (in tile space)
	/// @return pixels Uncompressed pixel bitmap
	/// @return url URL the tile should link to
	/// @return owner Tile owner
	/// @return approved Address approved to make 1 change to the tile
	/// @return next ID of the next tile in this tile's linked list
	/// @return prev ID of the previous tile in this tile's linked list
	function getTile(uint256 page, uint256 x, uint256 y) external view returns (
		bytes32[TILE_STORAGE_SIZE] memory pixels,
		string memory url,
		string memory title,
		address owner,
		address approved,
		uint256 price,
		uint256 next,
		uint256 prev
	)
	{
		uint256 tileId = (page << 16) | ((y & 0xFF) << 8) | (x & 0xFF);

		// Get pixels
		for(uint256 i = 0; i < TILE_STORAGE_SIZE; i++)
			pixels[i] = tilePixels[tileId][i];

		// Is this tile in a group?
		uint256 groupId = tileId;
		if(tileGroup[tileId] != 0)
			groupId = tileGroup[tileId];

		url      = tileUrl[groupId];
		title    = tileTitle[groupId];
		owner    = tileOwner[tileId];
		approved = tileApproval[tileId];
		price    = tilePrice[tileId];
		next     = tileNext[tileId];
		prev     = tilePrev[tileId];
	}

	/// @notice Get a tile by ID
	/// @param tileId ID of the tile
	/// @return pixels Uncompressed pixel bitmap
	/// @return url URL the tile should link to
	/// @return owner Tile owner
	/// @return approved Address approved to make 1 change to the tile
	/// @return next ID of the next tile in this tile's linked list
	/// @return prev ID of the previous tile in this tile's linked list
	function getTile(uint256 tileId) external view returns (
		bytes32[TILE_STORAGE_SIZE] memory pixels,
		string memory url,
		string memory title,
		address owner,
		address approved,
		uint256 price,
		uint256 next,
		uint256 prev
	)
	{
		// Get pixels
		for(uint256 i = 0; i < TILE_STORAGE_SIZE; i++)
			pixels[i] = tilePixels[tileId][i];

		// Is this tile in a group?
		uint256 groupId = tileId;
		if(tileGroup[tileId] != 0)
			groupId = tileGroup[tileId];

		url      = tileUrl[groupId];
		title    = tileTitle[groupId];
		owner    = tileOwner[tileId];
		approved = tileApproval[tileId];
		price    = tilePrice[tileId];
		next     = tileNext[tileId];
		prev     = tilePrev[tileId];
	}

	/// @notice Get the ID of the first tile in the linked tile list of the
	///  owner
	/// @param owner Tile owner
	/// @return firstTileId ID of the first tile in the list
	function firstTileID(
		address owner
	) external view returns (uint256 firstTileId)
	{
		return ownerFirstTile[owner];
	}

	/// @notice Get the ID of the next tile in the linked list
	/// @param tileId ID of the tile
	/// @return nextTileId ID of the next tile in the list
	function nextTileID(
		uint256 tileId
	) external view returns (uint256 nextTileId)
	{
		// Return tile
		return tileNext[tileId];
	}

	/// @notice Get the ID of the previous tile in the linked list
	/// @param tileId ID of the tile
	/// @return prevTileId ID of the previous tile in the list
	function prevTileID(
		uint256 tileId
	) external view returns (uint256 prevTileId)
	{
		// Return tile
		return tilePrev[tileId];
	}

	/// @notice Check if tile at the given location is claimed
	/// @param page page
	/// @param x x coordinate of the tile (in tile space)
	/// @param y y coordinate of the tile (in tile space)
	/// @return inUse true if the tile is claimed
	function isTileInUse(
		uint256 page,
		uint256 x,
		uint256 y
	) external view returns (bool inUse)
	{
		uint256 tileId = (page << 16) | ((y & 0xFF) << 8) | (x & 0xFF);

		return !(tileOwner[tileId] == address(0));
	}

	/// @notice Check if tile is claimed
	/// @param tileId ID of the tile
	/// @return inUse true if the tile is claimed
	function isTileInUse(uint256 tileId) external view returns (bool inUse)
	{
		return !(tileOwner[tileId] == address(0));
	}

	//--------------------------------------
	//
	// EtherPixel public functions
	//
	//--------------------------------------

	/// @notice Buy an unclaimed or for-sale tile for yourself
	/// @param page Page number
	/// @param x X coordinate of the tile (in tile space)
	/// @param y Y coordinate of the tile (in tile space)
	function buyTile(uint256 page, uint256 x, uint256 y) external payable
	{
		uint256 tileId = (page << 16) | ((y & 0xFF) << 8) | (x & 0xFF);
		buyTile(tileId, msg.sender);
	}

	/// @notice Buy an unclaimed or for-sale tile
	/// @param page Page number
	/// @param x X coordinate of the tile (in tile space)
	/// @param y Y coordinate of the tile (in tile space)
	/// @param to Address to be assigned as the new owner
	function buyTile(
		uint256 page,
		uint256 x,
		uint256 y,
		address to
	) external payable
	{
		uint256 tileId = (page << 16) | ((y & 0xFF) << 8) | (x & 0xFF);
		buyTile(tileId, to);
	}

	/// @notice Buy an unclaimed or for-sale tile for yourself
	/// @param tileId ID of the tile you wish to buy
	function buyTile(uint256 tileId) external payable
	{
		buyTile(tileId, msg.sender);
	}

	/// @notice Buy an unclaimed or for-sale tile
	/// @param tileId ID of the tile you wish to buy
	/// @param to Address to be assigned as the new owner
	function buyTile(uint256 tileId, address to) public payable
	{
		// Cannot buy to zero address
		require(
			to != address(0),
			"Zero address cannot be receiver"
		);

		uint256 x = tileId & 0xFF;
		uint256 y = (tileId >> 8) & 0xFF;
		uint256 page = tileId >> 16;

		// Make sure the tile we want to buy is within bounds
		require(
			x < PAGE_SIZE && y < PAGE_SIZE,
			"Tile is out of bounds."
		);

		// Make sure the page is within bounds
		// pageCount + 1 is allowed to allow users to create new pages
		// on demand
		require(
			page > 0 && page < _pageCount + 1,
			"Page is out of bounds."
		);

		bool tileIsForSale = tilePrice[tileId] > 0;

		// Tile must be unclaimed or for sale
		address oldOwner = tileOwner[tileId];
		require(
			oldOwner == address(0) ||
			tileIsForSale,
			"Tile is already claimed."
		);

		// Get price of the tile
		uint256 priceOfTile = _price;
		if(tilePrice[tileId] > 0)
			priceOfTile = tilePrice[tileId];

		// Did the user pay enough?
		require(
			msg.value == priceOfTile,
			"Incorrect amount of ether."
		);

		// Everything checked out. Transfer ownership of the tile!
		tileOwner[tileId] = to;
		ownerTileCount[to] = ownerTileCount[to] + 1;
		_addTokenToList(to, tileId);

		// If this was a user-to-user sale, send the old owner their ether
		if(tileIsForSale && oldOwner != address(0))
		{
			uint256 amountToTransfer = priceOfTile;

			// Should we keep a percentage?
			if(saleFeeRate > 0)
			{
				uint256 fee = amountToTransfer / saleFeeRate;
				amountToTransfer = amountToTransfer - fee;
			}

			// Transfer the tile
			_doTransfer(tileId, oldOwner, to);

			// Hack because of stupid desicions made in Solidity v0.5.0
			address payable etherReceiver = address(uint160(oldOwner));

			// Send the ether to the old owner
			etherReceiver.transfer(amountToTransfer);
		}
		else
		{
			// "Create" the tile
			emit Transfer(oldOwner, to, tileId);
		}
	}

	/// @notice Mark a pixel "for sale" by giving it a price tag. Set to 0 to
	///  mark it "not for sale".
	/// @param tileId ID of the tile you wish set the price of
	/// @param price Price of the tag in wei. Set to 0 to mark it not for sale.
	function setPrice(uint256 tileId, uint256 price) external
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Is the sender authorised?
		require(
				msg.sender == tileOwnerAddress ||
				operatorApprovals[tileOwnerAddress][msg.sender] ||
				tileApproval[tileId] == msg.sender,
				"You are not authorised."
		);

		tilePrice[tileId] = price;

		emit Modified(msg.sender, tileId, "price");
	}

	/// @notice Set the group of a tile, to make it share the URL and title of
	///  another tile.
	/// @param tileId ID of the tile to set the group of
	/// @param masterTileId ID of the tile to use for our URL and title.
	function setGroup(uint256 tileId, uint256 masterTileId) external
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Is the sender authorised?
		require(
				msg.sender == tileOwnerAddress ||
				operatorApprovals[tileOwnerAddress][msg.sender] ||
				tileApproval[tileId] == msg.sender,
				"You are not authorised."
		);

		tileGroup[tileId] = masterTileId;

		emit Modified(msg.sender, tileId, "group");
	}

	/// @notice Set the url of a tile.
	/// @param tileId ID of the tile to set the url of
	/// @param url The URL to attach
	function setUrl(uint256 tileId, string calldata url) external
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Is the sender authorised?
		require(
				msg.sender == tileOwnerAddress ||
				operatorApprovals[tileOwnerAddress][msg.sender] ||
				tileApproval[tileId] == msg.sender,
				"You are not authorised."
		);

		tileUrl[tileId] = url;

		emit Modified(msg.sender, tileId, "url");
	}

	/// @notice Set the title of a tile.
	/// @param tileId ID of the tile to set the title of
	/// @param title The title to attach
	function setTitle(uint256 tileId, string calldata title) external
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Is the sender authorised?
		require(
				msg.sender == tileOwnerAddress ||
				operatorApprovals[tileOwnerAddress][msg.sender] ||
				tileApproval[tileId] == msg.sender,
				"You are not authorised."
		);

		tileTitle[tileId] = title;

		emit Modified(msg.sender, tileId, "title");
	}

	/// @notice Set the pixels of a tile.
	/// @param tileId ID of the tile to set the pixels of
	/// @param pixels Raw array of tiles to set
	function setPixels(
		uint256 tileId,
		bytes32[TILE_STORAGE_SIZE] calldata pixels
	) external
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Is the sender authorised?
		require(
				msg.sender == tileOwnerAddress ||
				operatorApprovals[tileOwnerAddress][msg.sender] ||
				tileApproval[tileId] == msg.sender,
				"You are not authorised."
		);

		for(uint256 i = 0; i < TILE_STORAGE_SIZE; i++)
			tilePixels[tileId][i] = pixels[i];

		emit Modified(msg.sender, tileId, "pixels");
	}

	//--------------------------------------
	//
	// ERC165 functions
	//
	//--------------------------------------

	/// @notice Query if a contract implements an interface
	/// @param interfaceID The interface identifier, as specified in ERC-165
	/// @dev Interface identification is specified in ERC-165. This function
	///  uses less than 30,000 gas.
	/// @return `true` if the contract implements `interfaceID` and
	///  `interfaceID` is not 0xffffffff, `false` otherwise
	function supportsInterface(bytes4 interfaceID) external view returns (bool)
	{
		return (
			interfaceID == InterfaceID_ERC165 ||
			interfaceID == InterfaceID_ERC721 ||
			interfaceID == InterfaceID_ERC721Metadata
		);
	}

	//--------------------------------------
	//
	// ERC721 functions
	//
	//--------------------------------------

	/// @notice Count all NFTs assigned to an owner
	/// @dev NFTs assigned to the zero address are considered invalid, and this
	///  function throws for queries about the zero address.
	/// @param owner An address for whom to query the balance
	/// @return The number of NFTs owned by `owner`, possibly zero
	function balanceOf(address owner) external view returns (uint256 balance)
	{
		require(
			owner != address(0),
			"Cannot query zero address"
		);

		return ownerTileCount[owner];
	}

	/// @notice Find the owner of an NFT
	/// @dev NFTs assigned to zero address are considered invalid, and queries
	///  about them do throw.
	/// @param tileId The identifier for an NFT
	/// @return The address of the owner of the NFT
	function ownerOf(uint256 tileId) external view returns (address owner)
	{
		require(
			tileOwner[tileId] != address(0),
			"Invalid tile."
		);

		return tileOwner[tileId];
	}

	/// @notice Transfers the ownership of an NFT from one address to another
	///  address
	/// @dev Throws unless `msg.sender` is the current owner, an authorized
	///  operator, or the approved address for this NFT. Throws if `from` is
	///  not the current owner. Throws if `to` is the zero address. Throws if
	///  `tileId` is not a valid NFT. When transfer is complete, this function
	///  checks if `to` is a smart contract (code size > 0). If so, it calls
	///  `onERC721Received` on `to` and throws if the return value is not
	///  `bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"))`.
	/// @param from The current owner of the NFT
	/// @param to The new owner
	/// @param tileId The NFT to transfer
	/// @param data Additional data with no specified format, sent in call to
	///  `_to`
	function safeTransferFrom(
		address from,
		address to,
		uint256 tileId,
		bytes calldata data
	)external payable
	{
		_transfer(from, to, tileId, data, true);
	}

	/// @notice Transfers the ownership of an NFT from one address to another
	///  address
	/// @dev This works identically to the other function with an extra data
	///  parameter, except this function just sets data to ""
	/// @param from The current owner of the NFT
	/// @param to The new owner
	/// @param tileId The NFT to transfer
	function safeTransferFrom(
		address from,
		address to,
		uint256 tileId
	) external payable
	{
		_transfer(from, to, tileId, "", true);
	}

	/// @notice Transfer ownership of an NFT -- THE CALLER IS RESPONSIBLE
	///  TO CONFIRM THAT `_to` IS CAPABLE OF RECEIVING NFTS OR ELSE
	///  THEY MAY BE PERMANENTLY LOST
	/// @dev Throws unless `msg.sender` is the current owner, an authorized
	///  operator, or the approved address for this NFT. Throws if `from` is
	///  not the current owner. Throws if `_to` is the zero address. Throws if
	///  `tileId` is not a valid NFT.
	/// @param from The current owner of the NFT
	/// @param to The new owner
	/// @param tileId The NFT to transfer
	function transferFrom(
		address from,
		address to,
		uint256 tileId
	) external payable
	{
		_transfer(from, to, tileId, "", false);
	}

	/// @notice Set or reaffirm the approved address for an NFT
	/// @dev The zero address indicates there is no approved address.
	/// @dev Throws unless `msg.sender` is the current NFT owner, or an
	///  authorized operator of the current owner.
	/// @param approved The new approved NFT controller
	/// @param tileId The NFT to approve
	function approve(address approved, uint256 tileId) external payable
	{
		address tileOwnerAddress = tileOwner[tileId];
		require(
			tileOwnerAddress != address(0),
			"Invalid tile."
		);

		require(
			tileOwnerAddress == msg.sender ||
			operatorApprovals[tileOwnerAddress][msg.sender],
			"You are not authorised."
		);

		tileApproval[tileId] = approved;

		emit Approval(tileOwnerAddress, approved, tileId);
	}

	/// @notice Enable or disable approval for a third party ("operator") to
	///  manage all of `msg.sender`'s assets.
	/// @dev Emits the ApprovalForAll event. The contract MUST allow
	///  multiple operators per owner.
	/// @param operator Address to add to the set of authorized operators.
	/// @param approved True if the operator is approved, false to revoke
	///  approval
	function setApprovalForAll(address operator, bool approved) external
	{
		require(
			operator != msg.sender,
			"You cannot approve yourself."
		);

		operatorApprovals[msg.sender][operator] = approved;

		emit ApprovalForAll(msg.sender, operator, approved);
	}

	/// @notice Get the approved address for a single NFT
	/// @dev Throws if `tileId` is not a valid NFT
	/// @param tileId The NFT to find the approved address for
	/// @return The approved address for this NFT, or the zero address if there
	///  is none
	function getApproved(uint256 tileId) external view returns (address)
	{
		// Make sure the tile is valid (Only owned tiles are valid)
		require(
			tileOwner[tileId] != address(0),
			"Invalid tile."
		);

		return tileApproval[tileId];
	}

	/// @notice Query if an address is an authorized operator for another
	///  address
	/// @param owner The address that owns the NFTs
	/// @param operator The address that acts on behalf of the owner
	/// @return True if `operator` is an approved operator for `owner`, false
	///  otherwise
	function isApprovedForAll(
		address owner,
		address operator
	) external view returns (bool isApproved)
	{
		return operatorApprovals[owner][operator];
	}

	//--------------------------------------
	//
	// ERC721Metadata Functions
	//
	//--------------------------------------

	/// @notice A descriptive name for a collection of NFTs in this contract
	function name() external view returns (string memory _name)
	{
		return ERC721MetadataName;
	}

	/// @notice An abbreviated name for NFTs in this contract
	function symbol() external view returns (string memory _symbol)
	{
		return ERC721MetadataSymbol;
	}

	/// @notice A distinct Uniform Resource Identifier (URI) for a given asset.
	/// @dev Throws if `tileId` is not a valid NFT. URIs are defined in RFC
	///  3986. The URI may point to a JSON file that conforms to the "ERC721
	///  Metadata JSON Schema".
	function tokenURI(uint256 tileId) external view returns (string memory url)
	{
		// Make sure the tile is valid (Only owned tiles are valid)
		require(
			tileOwner[tileId] != address(0),
			"Invalid tile."
		);

		// Flip the tile ID, to simplify the hex conversion logic
		uint256 id = tileId;
		uint256 flippedId = 0;
		uint256 idLength = 0;
		while(id != 0)
		{
			flippedId = (flippedId << 4) | (id & 0xF);
			id = id >> 4;
			idLength++;
		}

		// Get the base URL
		bytes memory baseUrl = bytes(ERC721MetadataUrl);
		bytes memory metaUrl = bytes(new string(baseUrl.length + idLength));
		uint256 index = baseUrl.length;
		for(uint256 i = 0; i < index; i++)
			metaUrl[i] = baseUrl[i];

		// Convert the tile ID to hex string and append to the URL
		for(uint256 i = 0; i < idLength; i++)
		{
			uint8 nibble = uint8(flippedId & 0xF);
			flippedId = flippedId >> 4;
			if(nibble >= 0x0 && nibble < 0xA)
				metaUrl[index++] = byte(nibble + 48);
			else
				metaUrl[index++] = byte(nibble - 0xA + 97);
		}

		return string(metaUrl);
	}

	//--------------------------------------
	//
	// Restricted functions
	//
	//--------------------------------------

	// Let operator release pixels
	function release(uint256 tokenId) external
	{
		// Must be contract operator
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		// Tile must have an owner - Cannot release free tiles
		address owner = tileOwner[tokenId];
		require(
			owner != address(0),
			"Invalid tile."
		);

		// Free the tile
		tileOwner[tokenId] = address(0);
		_removeTokenFromList(tokenId);

		// Reset approval
		tileApproval[tokenId] = address(0);

		// """Destroy"""
		emit Transfer(owner, address(0), tokenId);
	}

	// Let operator claim pixels
	function claim(uint256 tokenId, address to) external
	{
		// Must be contract operator
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		// Tile must not have an owner
		require(
			tileOwner[tokenId] == address(0),
			"Tile is already claimed."
		);

		// Claim the tile
		tileOwner[tokenId] = to;
		ownerTileCount[to] = ownerTileCount[to] + 1;
		_addTokenToList(to, tokenId);

		// """Create"""
		emit Transfer(address(0), to, tokenId);
	}

	// Let operator change tile owner
	function changeTileOwner(uint256 tokenId, address newOwner) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		require(
			tileOwner[tokenId] != address(0),
			"Tile is unclaimed."
		);

		// Transfer
		_doTransfer(tokenId, tileOwner[tokenId], newOwner);
	}

	// Let operator set new operator
	function setOperator(address newOperator) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		contractOperator = newOperator;
	}

	// Set ERC721Metadata name
	function setMetaName(string calldata _name) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		ERC721MetadataName = _name;
	}

	// Set ERC721Metadata name
	function setMetaSymbol(string calldata _symbol) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		ERC721MetadataSymbol = _symbol;
	}

	// Set ERC721Metadata url
	function setMetaUrl(string calldata url) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		ERC721MetadataUrl = url;
	}

	// Set default price
	function setPrice(uint256 price) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		_price = price;
	}

	// Let operator widthdraw funds
	function widthdrawFunds(uint256 amount, address payable to) external
	{
		require(
			msg.sender == contractOperator,
			"You are not authorised."
		);

		to.transfer(amount);
	}

	//--------------------------------------
	//
	// Internal functions
	//
	//--------------------------------------

	// Transfer tile from A to B in a ERC721-compatible fashion
	function _transfer(
		address from,
		address to,
		uint256 tileId,
		bytes memory data,
		bool safe
	) internal
	{
		address tileOwnerAddress = tileOwner[tileId];

		// Make sure the tile is valid (Only owned tiles are valid)
		require(
			tileOwnerAddress != address(0),
			"Invalid tile."
		);

		// Make sure receiver is valid
		require(
			to != address(0),
			"Invalid receiver."
		);

		// Throw if:
		// 1) Sender is not owner
		// 2) Sender is not operator
		// 3) From is not tile owner
		if((
				msg.sender != tileOwnerAddress &&
				!operatorApprovals[tileOwnerAddress][msg.sender] &&
				tileApproval[tileId] != msg.sender
			) || from != tileOwnerAddress
		)
			revert("You are not authorised.");

		// Internal transfer
		_doTransfer(tileId, from, to);

		// If this is a safe transfer, we must check if it's being sent to a
		// contract
		if(safe)
		{
			// Is receiver a contract? (Contract have non-zero code sizes)
			uint256 extCodeSize = 0;
			assembly
			{
				extCodeSize := extcodesize(to)
			}
			if(extCodeSize > 0)
			{
				// Attempt to call the receiving contract
				ERC721TokenReceiver toContract = ERC721TokenReceiver(to);
				bytes4 result = toContract.onERC721Received(
					msg.sender,
					from,
					tileId,
					data
				);

				// Did the receiving contract respond appropriately?
				require(
					result == ERC721_RECEIVED,
					"Invalid contract receiver."
				);
			}
		}
	}

	// Internal transfer routine
	function _doTransfer(uint256 tileId, address from, address to) internal
	{
		// Make sure the transfer is possible
		require(
			ownerTileCount[from] != 0,
			"Illegal transfer."
		);

		// Do transfer
		tileOwner[tileId] = to;
		ownerTileCount[to] = ownerTileCount[to] + 1;
		ownerTileCount[from] = ownerTileCount[from] - 1;
		_removeTokenFromList(tileId);
		_addTokenToList(to, tileId);

		// Reset approval
		tileApproval[tileId] = address(0);

		emit Transfer(from, to, tileId);
	}

	// Add token to the new owner's list of tiles
	function _addTokenToList(address owner, uint256 tileId) internal
	{
		// Add tile to the buyer's list of tokens
		uint256 firstTileId = ownerFirstTile[owner];
		if(firstTileId != 0)
		{
			// Add to existing list
			tilePrev[firstTileId] = tileId;
			tileNext[tileId] = firstTileId;
		}
		else
		{
			// New list
			ownerFirstTile[owner] = tileId;
		}
	}

	// Remove token from the old owner's list of tiles
	function _removeTokenFromList(uint256 tileId) internal
	{
		// If there is a tile in front of this
		if(tileNext[tileId] != 0)
		{
			uint256 nextTile = tileNext[tileId];

			// If we have a tile behind us, attach the tile in front of us to
			// the tile behind us
			if(tilePrev[tileId] != 0)
				tileNext[nextTile] = tilePrev[tileId];
			// Otherwise mark the next tile as the first tile
			else
				tileNext[nextTile] = 0;
		}
		// If there is a tile behind us
		if(tilePrev[tileId] != 0)
		{
			uint256 prevTile = tilePrev[tileId];

			// If we have a tile in front of us us, attach the tile behind us to
			// the tile in front of us
			if(tileNext[tileId] != 0)
				tilePrev[prevTile] = tileNext[tileId];
			// Otherwise mark the tile behind us as the last tile
			else
				tilePrev[prevTile] = 0;
		}
	}
}

//------------------------------------------------------------------------------