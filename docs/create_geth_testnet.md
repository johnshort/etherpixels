Creating a local test network with Geth
=======================================

Testing is important, but you may not want to litter other public testnets with your debugging contracts. Instead, just create a brand spanking new, local test network! Here's how.

Step 1
------
Create a new test network

```
geth --identity "MyFirstBlockchain" --nodiscover --networkid 9999 --datadir /path/to/datadir init /path/to/Genesis.json
```

Step 2
------
Create a new account:

```
geth account new --datadir /path/to/datadir
```

Step 3
------
The above command will give you a new address. Take that address and replace it with the one found in `Genesis.json`.

Step 4
------
Delete the test network you created earlier

```
geth removedb --datadir /path/to/datadir
```

Step 5
------
Create the test network again with the updated `Genesis.json` file.

```
geth --identity "MyFirstBlockchain" --nodiscover --networkid 9999 --datadir /path/to/datadir init /path/to/Genesis.json
```

Step 6
------
Run your local geth node.

```
geth --identity "MyFirstBlockchain" --verbosity 4 --ipcdisable --nodiscover --networkid 9999 --unlock 0 --rpc --rpcport "8545" --rpcaddr 127.0.0.1 --rpccorsdomain "*" --rpcvhosts "*" --rpcapi "eth,web3,personal,miner,net,txpool,admin,debug,miner,personal" --datadir /path/to/datadir --mine --etherbase '0xbc2338ec40b1a98ae523379ba89d94b46d028d26' --minerthreads=1 console
```

Remember to change the `--etherbase` option with the address you created earlier.

Note that this geth instance is *not secure*! We allow any connections from anyone and we permanently unlock our account. But it's okay. It's just a disposable testnet.
