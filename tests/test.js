
const Web3 = require('web3')
const Debug = require('web3-eth-debug').Debug
const fs = require('fs')

const host = 'localhost'
const port = 8545
const abiFile = 'build/etherpixels_sol_EtherPixels.abi'

const web3 = new Web3('http://'+host+':'+port)
const debug = new Debug('http://'+host+':'+port)

let abi = JSON.parse(fs.readFileSync(abiFile))

let deployedContractInfo = JSON.parse(fs.readFileSync('deployed.json', 'utf8'))[0]

let contractAddress = deployedContractInfo.address
let from = '0x7a9b96e3d1c31c3f7f88ad6e277ad02ad747a63f'

let contract = new web3.eth.Contract(abi, contractAddress)

function getTransactionError(tx, cb)
{
	debug.getTransactionTrace(tx, {}, (err, res) =>
	{
		console.log(res.returnValue)
		let size = Buffer.from(res.returnValue.substring(134, 136), 'hex')[0]
		let revertStr = Buffer.from(res.returnValue.substring(136, 136+(size*2)), 'hex').toString('ascii')
		console.log(revertStr)
		if(cb) cb(err, res, revertStr)
	})
}

function claimTile(tileId, cb)
{
	let tx = ''
	contract.methods.claim(tileId, from).send({from: from, gas: 500000}, (err, res) =>
	{
		tx = res
		console.log(err, res)
	}).on('error', (err, receipt) =>
	{
		console.log('Error')
		getTransactionError(tx, (err, res) =>
		{
			//cb(err, res)
		})
	});
}

function setPixels(tileId, pixels, cb)
{
	let tx = ''
	contract.methods.setPixels(tileId, pixels).send({from: from, gas: 1000000}, (err, res) =>
	{
		tx = res
		console.log(err, res)
	}).on('error', (err, receipt) =>
	{
		getTransactionError(tx, (err, res) =>
		{
			//cb(err, res)
		})
	});
}

function setUrl(tileId, url, cb)
{
	url = '0x'+Buffer.from(url).toString('hex')
	let tx = ''
	contract.methods.setUrl(tileId, url).send({from: from, gas: 1000000}, (err, res) =>
	{
		tx = res
		console.log(err, res)
	}).on('error', (err, receipt) =>
	{
		getTransactionError(tx, (err, res) =>
		{
			//cb(err, res)
		})
	});
}

function getTile(tileId, cb)
{
	let tx = ''
	contract.methods.getTile(tileId).call((err, res) =>
	{
		console.log(res)
		let url = Buffer.from(res.url.substring(2), 'hex').toString('utf8')
		console.log(url)
	})
}

function tokenURI(tileId, cb)
{
	let tx = ''

	contract.methods.tokenURI(tileId).call((err, res) =>
	{
		if(err) throw err
		console.log(res)
	})

	/*
	contract.methods.tokenURI(tileId).send({from: from, gas: 1000000}, (err, res) =>
	{
		tx = res
		console.log(err, res)
	}).on('error', (err, receipt) =>
	{
		getTransactionError(tx, (err, res) =>
		{
			//cb(err, res)
			console.log(res)
		})
	});
	*/
}

let pixels = []
for(let i = 0; i < 24; i++) pixels[i] = '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'

claimTile(0x010000)
//setPixels(1, pixels) // 560853
//setUrl(1, "https://www.youtube.com/asdasdasdasdasdasdasdasd&Yeeeeehaaaaa123")
//getTile(1)
//tokenURI(0xDEADBEEF)
