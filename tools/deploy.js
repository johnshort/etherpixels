//------------------------------------------------------------------------------
//
// deploy.js - Deploy contract(s)
//
// Copyright (C) 2019 Shibesoft AS
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//------------------------------------------------------------------------------

const Web3 = require('web3')
const fs = require('fs')

let args = process.argv.slice(2)

// Read options
let opts = {
	host: 'localhost',
	port: 8545,
	output: 'deployed.json'
}
while(args.length)
{
	let arg = args.shift()
	switch(arg)
	{
		case '-h':
			opts.host = args.shift()
			break

		case '-p':
			opts.post = parseInt(args.shift(), 10)
			break

		case '-a':
			opts.account = args.shift()
			break

		case '-o':
			opts.output = args.shift()
			break

		default:
			let f = arg.trim()
			if(f)
				opts.dir = f
	}
}

if(!opts.dir)
{
	console.log('Needs a directory of contracts to deploy')
	process.exit(1)
}

let web3 = new Web3('http://'+opts.host+':'+opts.port)

findContracts(opts.dir, (err, toDeploy) =>
{
	if(err)
	{
		console.log(err)
		process.exit(1)
	}
	if(toDeploy.length == 0)
	{
		console.log('No contracts found')
		process.exit(1)
	}

	deployAll(toDeploy, 0, (err, res) =>
	{
		if(err)
		{
			console.log(err)
			process.exit(1)
		}
		if(opts.output)
		{
			fs.writeFileSync(opts.output, JSON.stringify(res, null, 4))
		}
		console.log('Finished')
		process.exit(0)
	})

})

function deployAll(toDeploy, index, cb)
{
	if(index >= toDeploy.length)
		return cb(null, [])
	deploy(toDeploy[index], (err, deployRes) =>
	{
		if(err) return cb(err, null)
		deployAll(toDeploy, index + 1, (err, res) =>
		{
			if(err) return cb(err, null)
			res.push(deployRes)
			cb(null, res)
		})
	})
}

function findContracts(dir, cb)
{
	fs.readdir(dir, (err, list) =>
	{
		if(err) return cb(err, null)
		let toDeploy = []
		for(let i in list)
		{
			let extension = list[i].split('.').pop()
			if(extension == 'bin')
				toDeploy.push(dir+'/'+list[i])
		}
		cb(null, toDeploy)
	})
}

function getAccount(cb)
{
	if(opts.account) return cb(null, opts.account)
	web3.eth.getAccounts((err, accounts) =>
	{
		cb(err, accounts[0])
	})
}

function deploy(filename, cb)
{
	console.log('Deploying: '+filename)
	fs.readFile(filename, (err, contractBinary) =>
	{
		if(err) return cb(err, null)
		getAccount((err, account) =>
		{
			if(err) return cb(err, null)
			let call = {
				from: account,
				to: null,
				data: '0x'+contractBinary.toString('hex')
			}

			web3.eth.estimateGas(call, (err, estimatedGas) =>
			{
				if(err) return cb(err, null)
				console.log('Estimated gas: '+estimatedGas)
				call.gas = estimatedGas
				web3.eth.sendTransaction(call, (err, tx) =>
				{
					if(err) return cb(err, null)
					console.log('Transaction:   '+tx)
					getContractAddress(tx, (err, deploymentResult) =>
					{
						if(err) return cb(err, null)
						deploymentResult.contract = filename
						cb(null, deploymentResult)
					})
				})
			})
		})
	})
}

function getContractAddress(tx, cb)
{
	web3.eth.getTransactionReceipt(tx, (err, res) =>
	{
		if(err) return cb(err, null)

		// If no result, try again
		if(!res)
		{
			setTimeout(() =>
			{
				getContractAddress(tx, cb)
			}, 500)
			return
		}

		console.log('Result:        '+res.status)
		console.log('Gas used:      '+res.gasUsed)
		console.log('Address:       '+res.contractAddress)

		cb(null, {
			result: res.status,
			gasUsed: res.gasUsed,
			tx: tx,
			address: res.contractAddress
		})
	})
}
