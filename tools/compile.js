//------------------------------------------------------------------------------
//
// compile.js - Front-end to solc that gives us a more
//              command-line-friendly interface
//
// Copyright (C) 2019 Shibesoft AS
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
//------------------------------------------------------------------------------

const path = require('path')
const fs = require('fs')
const solc = require('solc')

let args = process.argv.slice(2)

// Read options
let opts = {
	files: []
}
while(args.length)
{
	let arg = args.shift()
	switch(arg)
	{
		case '-V':
			opts.version = args.shift()
			break

		case '-a':
			opts.assembly = true
			break

		case '-b':
			opts.abi = true
			break

		case '-o':
			opts.output = args.shift()
			break

		case '-O':
			opts.optimise = args.shift()
			break

		// Custom solc JSON file
		case '-j':
			opts.json = args.shift()
			break

		default:
			let f = arg.trim()
			if(f)
				opts.files.push(f)
	}
}

// solc is a compiler that can't read it's own files...
function readFile(file)
{
	try
	{
		let f = fs.readFileSync(file, 'utf8')
		return f
	}
	catch(e)
	{
		throw new Error('Could not import file: '+file+'. '+e.message)
	}
}

function build(opts)
{
	let solcInput = {
		language: 'Solidity',
		sources: {},
		settings: {
			outputSelection: {
				'*': {
					'*': [
						'evm.bytecode',
						'evm.methodIdentifiers',
						'evm.gasEstimates'
					]
				}
			}
		}
	}

	for(let i in opts.files)
		solcInput.sources[opts.files[i]] = {content: readFile(opts.files[i])}

	if(opts.optimise !== undefined)
	{
		let enabled = opts.optimise > 0
		solcInput.settings.optimizer = {
			enabled: enabled,
			runs: parseInt(opts.optimise, 10)
		}
	}

	if(opts.version)
		solcInput.settings.evmVersion = opts.version

	if(opts.assembly)
		solcInput.settings.outputSelection['*']['*'].push('evm.assembly')

	if(opts.abi)
		solcInput.settings.outputSelection['*']['*'].push('abi')

	if(opts.metadata)
		solcInput.settings.outputSelection['*']['*'].push('metadata')

	let output = solc.compileStandardWrapper(
		JSON.stringify(solcInput),
		readFile
	)

	let solcOutput = JSON.parse(output)
	let isError = false

	if(solcOutput.errors)
	{
		for(let i in solcOutput.errors)
		{
			let error = solcOutput.errors[i]
			console.log(
				error.severity+': '+
				error.component+': '+
				error.formattedMessage
			);
			if(error.severity == 'error')
				isError = true
		}
	}

	if(isError)
		return 1

	let o = './'
	if(opts.output)
		o = opts.output
	if(o[o.length - 1] != '/') o += '/'
	let jsonOutput = o+'build.json'
	fs.writeFileSync(jsonOutput, JSON.stringify(solcOutput, null, 4))

	let bin = ''
	for(let i in solcOutput.contracts)
	{
		let basename = o+i.split('/').pop().replace(/\./g, '_')
		for(let j in solcOutput.contracts[i])
		{
			let contract = solcOutput.contracts[i][j]
			let filename = basename+'_'+j.split('/').pop().replace(/\./g, '_')

			// Did the contract produce bytecode?
			if(
				contract.evm &&
				contract.evm.bytecode &&
				contract.evm.bytecode.object
			)
			{
				fs.writeFileSync(
					filename+'.bin',
					Buffer.from(contract.evm.bytecode.object, 'hex')
				)
			}

			// Was there any assembly output?
			if(
				contract.evm &&
				contract.evm.bytecode &&
				contract.evm.bytecode.object
			)
			{
				fs.writeFileSync(
					filename+'.asm',
					contract.evm.bytecode.opcodes
				)
			}

			// Was there any abi generater?
			if(contract.abi)
			{
				fs.writeFileSync(
					filename+'.abi',
					JSON.stringify(contract.abi)
				)
			}
		}
	}

	return 0
}

process.exit(build(opts))
