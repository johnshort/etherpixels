#-------------------------------------------------------------------------------
#
# EtherPixels makefile
#
# Copyright (C) 2019 Shibesoft AS
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#-------------------------------------------------------------------------------

TARGET := etherpixels
SRC    := src/etherpixels.sol

OUTPUT := build

CFLAGS  := -V petersburg -b -O 100000

RM     := rm -rf
CAT    := cat
NODE   := nodejs
SOLC   := $(NODE) tools/compile.js
DEPLOY := $(NODE) tools/deploy.js

all: $(TARGET)

clean:
	$(RM) $(OUTPUT)/*.abi
	$(RM) $(OUTPUT)/*.bin
	$(RM) $(OUTPUT)/*.asm
	$(RM) $(OUTPUT)/*.json

deploy:
	$(DEPLOY) $(OUTPUT)

$(TARGET):
	$(SOLC) -o $(OUTPUT) $(CFLAGS) $(SRC)

